﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnigmaMachine
{
    static class CollectionsWorker
    {
        private static Random random = new Random();

        public static void Shuffle<T>(IList<T> array)
        {
            for (int i = array.Count - 1; i >= 1; --i)
            {
                int j = random.Next(i + 1);
                T val = array[i];
                array[i] = array[j];
                array[j] = val;
            }
        }
    }
}
