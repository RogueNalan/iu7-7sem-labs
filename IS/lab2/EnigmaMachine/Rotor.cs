﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnigmaMachine
{
    sealed class Rotor
    {
        private const int size = byte.MaxValue + 1;

        private readonly IList<byte> subst;

        private readonly IList<byte> revSubst;

        private int rotations = 0;

        public Rotor Next { get; set; } = null;

        public Rotor(Rotor next = null)
        {
            var subst = Enumerable.Range(0, size).Select(x => (byte)x).ToArray();
            CollectionsWorker.Shuffle(subst);
            var revSubst = new byte[size];
            for (int i = 0; i < size; ++i)
                revSubst[subst[i]] = (byte)i;

            this.subst = subst;
            this.revSubst = revSubst;
            this.Next = next;
        }

        public void Rotate()
        {
            if (++rotations >= size)
            {
                rotations = 0;
                Next?.Rotate();
            }
        }

        public void ClearRotations()
        {
            rotations = 0;
        }

        public byte Encrypt(byte src)
        {
            return subst[(rotations + src) % size];
        }

        public byte EncryptBack(byte src)
        {
            return (byte)((revSubst[src] + size - rotations) % size);
        }
    }
}
