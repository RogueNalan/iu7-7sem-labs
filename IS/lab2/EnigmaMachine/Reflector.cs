﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnigmaMachine
{
    sealed class Reflector
    {
        private const int size = byte.MaxValue + 1;

        private readonly IList<byte> subst;

        public Reflector()
        {
            int half = size / 2;
            var subst = Enumerable.Range(half, half).Select(x => (byte)x).ToList();
            CollectionsWorker.Shuffle(subst);
            subst.AddRange(Enumerable.Repeat(0, half).Select(x => (byte)x));
            for (byte i = 0; i < half; ++i)
                subst[subst[i]] = i;

            this.subst = subst;
        }

        public byte Process(byte src)
        {
            return subst[src];
        }
    }
}
