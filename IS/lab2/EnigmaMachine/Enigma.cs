﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnigmaMachine
{
    public class Enigma
    {
        private readonly Rotor[] rotors;

        private readonly Reflector reflector;

        public Enigma(int rotorsCount)
        {
            if (rotorsCount <= 0)
                throw new ArgumentOutOfRangeException("rotorsCount");

            rotors = new Rotor[rotorsCount];
            rotors[0] = new Rotor();
            for (int i = 1; i < rotors.Length; ++i)
                rotors[i] = new Rotor(rotors[i - 1]);
            if (rotorsCount != 1)
                rotors[0].Next = rotors[rotorsCount - 1];
            reflector = new Reflector();
        }

        public byte Process(byte src)
        {
            for (int i = rotors.Length - 1; i >= 0; --i)
                src = rotors[i].Encrypt(src);
            src = reflector.Process(src);
            for (int i = 0; i < rotors.Length; ++i)
                src = rotors[i].EncryptBack(src);
            rotors[rotors.Length - 1].Rotate();
            return src;
        }

        public IEnumerable<byte> Process(IEnumerable<byte> src)
        {
            return src?.Select(x => Process(x));
        }

        public void ClearRotations()
        {
            foreach (var rotor in rotors)
                rotor.ClearRotations();
        }
    }
}
