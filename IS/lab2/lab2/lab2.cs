﻿using EnigmaMachine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2
{
    public partial class lab2 : Form
    {
        private Enigma enigma;

        private const int bufSize = 4096;

        public lab2()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            enigma = new Enigma((int)nudRotors.Value);
            btnProcessFile.Enabled = true;
            btnClear.Enabled = true;
        }

        private void btnProcessFile_Click(object sender, EventArgs e)
        {
            try
            {
                var ofd = new OpenFileDialog();
                var sfd = new SaveFileDialog();
                if (ofd.ShowDialog() == DialogResult.OK && sfd.ShowDialog() == DialogResult.OK)
                {
                    using (var ofs = new FileStream(ofd.FileName, FileMode.Open))
                    using (var sfs = new FileStream(sfd.FileName, FileMode.Create))
                    {
                        var buffer = new byte[bufSize];
                        int read = -1;
                        while (read != 0)
                        {
                            read = ofs.Read(buffer, 0, bufSize);
                            IEnumerable<byte> temp;
                            if (read == bufSize)
                                temp = enigma.Process(buffer);
                            else
                                temp = enigma.Process(buffer.Take(read));
                            var res = temp.ToArray();
                            sfs.Write(res, 0, res.Length);
                        }
                    }
                    MessageBox.Show("Операция прошла успешно!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            enigma.ClearRotations();
        }
    }
}
