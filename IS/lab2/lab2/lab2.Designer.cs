﻿namespace lab2
{
    partial class lab2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.nudRotors = new System.Windows.Forms.NumericUpDown();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnProcessFile = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudRotors)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Количество роторов:";
            // 
            // nudRotors
            // 
            this.nudRotors.Location = new System.Drawing.Point(133, 13);
            this.nudRotors.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudRotors.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nudRotors.Name = "nudRotors";
            this.nudRotors.Size = new System.Drawing.Size(120, 20);
            this.nudRotors.TabIndex = 1;
            this.nudRotors.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(260, 9);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 2;
            this.btnCreate.Text = "Создать";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnProcessFile
            // 
            this.btnProcessFile.Enabled = false;
            this.btnProcessFile.Location = new System.Drawing.Point(12, 39);
            this.btnProcessFile.Name = "btnProcessFile";
            this.btnProcessFile.Size = new System.Drawing.Size(151, 23);
            this.btnProcessFile.TabIndex = 3;
            this.btnProcessFile.Text = "Обработать файл";
            this.btnProcessFile.UseVisualStyleBackColor = true;
            this.btnProcessFile.Click += new System.EventHandler(this.btnProcessFile_Click);
            // 
            // btnClear
            // 
            this.btnClear.Enabled = false;
            this.btnClear.Location = new System.Drawing.Point(170, 39);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(165, 23);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "Сброить повороты";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lab2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 74);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnProcessFile);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.nudRotors);
            this.Controls.Add(this.label1);
            this.Name = "lab2";
            this.Text = "lab2";
            ((System.ComponentModel.ISupportInitialize)(this.nudRotors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudRotors;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Button btnProcessFile;
        private System.Windows.Forms.Button btnClear;
    }
}

