﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SystemDataGetter
{
    public static class DataGetter
    {
        public static string GetData()
        {
            var scope = new ManagementScope("\\\\" + Environment.MachineName + "\\root\\cimv2");
            scope.Connect();
            var wmiClass = new ManagementObject(scope, new ManagementPath("Win32_BaseBoard.Tag=\"Base Board\""), new ObjectGetOptions());
            string strData = null;
            foreach (PropertyData p in wmiClass.Properties)
            {
                if (p.Name == "SerialNumber")
                    strData = p.Value.ToString();
            }
            return strData;
        }
    }
}
