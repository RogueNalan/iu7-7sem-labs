﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using SystemDataGetter;
using static SystemDataGetter.DataGetter;

namespace lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var path = Assembly.GetExecutingAssembly().Location;
                var asm = Assembly.LoadFile(Path.Combine(Path.GetDirectoryName(path), "data.dll"));
                var type = asm.GetType("IdentData.Data");
                var field = type.GetField("mboardid", BindingFlags.Static | BindingFlags.Public);
                string res = (string)field.GetValue(null);
                if (res == DataGetter.GetData())
                    Console.WriteLine("Success!");
                else
                    return;
                Console.ReadLine();
            }
            catch (Exception)
            {
                return;
            }
        }
    }
}
