﻿using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using SystemDataGetter;

namespace Inst
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var id = DataGetter.GetData();
                var dict = new Dictionary<string, string>();
                dict.Add("mboardid", id);
                CreateAssembly(dict);
                Install();
                Console.WriteLine("Success!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static void CreateAssembly(IDictionary<string, string> data)
        {
            using (var prov = new CSharpCodeProvider())
            {
                var param = new CompilerParameters()
                {
                    GenerateExecutable = false,
                    OutputAssembly = "data.dll"
                };
                string source =
                    @"
namespace IdentData
{
    public static class Data
    {
    ";
                foreach (var pair in data)
                    source += "public static readonly string " + pair.Key + " = \"" + pair.Value + "\";\n";
                source += "}}";
                var res = prov.CompileAssemblyFromSource(param, new string[] { source });
            }
        }

        private static void Install()
        {
            var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "lab1");
            Directory.CreateDirectory(path);
            File.Copy("lab1.exe", Path.Combine(path, "lab1.exe"), true);
            File.Copy("SystemDataGetter.dll", Path.Combine(path, "SystemDataGetter.dll"), true);
            File.Copy("data.dll", Path.Combine(path, "data.dll"), true);
        }
    }
}
