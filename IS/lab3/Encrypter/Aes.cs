﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Encrypter.AesData;

namespace Encrypter
{
    public sealed class Aes
    {
        private const int Nb = 4;

        private readonly int Nk, Nr;

        private readonly byte[][] expKey;

        public Aes(byte[] key)
        {
            if (key == null)
                throw new ArgumentNullException("key");

            switch (key.Length)
            {
                case 16:
                    Nk = 4;
                    Nr = 10;
                    break;
                case 24:
                    Nk = 6;
                    Nr = 12;
                    break;
                case 32:
                    Nk = 8;
                    Nr = 14;
                    break;
                default:
                    throw new ArgumentException("key");
            }
            expKey = KeyExpansion(key);
        }

        public byte[] Encode(byte[] buf)
        {
            int len = buf.Length;
            int blocksize = Nb * Nb;
            if (len == 0) return buf;
            var encbuf = BitConverter.GetBytes(len).Concat(buf);
            int newlen = encbuf.Count();
            int tail = newlen % (blocksize);
            if (tail != 0)
            {
                byte[] z = new byte[blocksize - tail];
                Array.Clear(z, 0, z.Length);
                encbuf = encbuf.Concat(z);
                newlen = encbuf.Count();
            }
            IEnumerable<byte> res = new byte[0];
            for (int i = 0; i < newlen / blocksize; i++)
            {
                res = res.Concat(Encrypt(encbuf.Skip(i * blocksize).Take(blocksize).ToArray()));
            }
            return res.ToArray();
        }

        public void Encode(BinaryReader re, BinaryWriter wr, int len)
        {
            int blocksize = Nb * Nb;
            if (len == 0) return;
            byte[] encbuf = BitConverter.GetBytes(len);
            int newlen = len + 4;
            int tail = newlen % (blocksize);

            byte[] tmp = new byte[blocksize];
            Array.Clear(tmp, 0, blocksize);
            encbuf.CopyTo(tmp, 0);
            re.Read(tmp, 4, blocksize - 4);
            wr.Write(Encrypt(tmp));
            for (int i = 1; i < newlen / blocksize; i++)
            {
                re.Read(tmp, 0, blocksize);
                wr.Write(Encrypt(tmp));
            }
            if (tail != 0 && newlen >= blocksize)
            {
                Array.Clear(tmp, 0, blocksize);
                re.Read(tmp, 0, tail);
                wr.Write(Encrypt(tmp));
            }
        }

        public void Encode(Stream src, Stream dst)
        {
            using (var re = new BinaryReader(src))
            using (var wr = new BinaryWriter(dst))
                Encode(re, wr, (int)src.Length);
        }

        public byte[] Decode(byte[] buf)
        {
            int len = buf.Length;
            int blocksize = Nb * Nb;
            if (len % blocksize != 0)
                return null;
            IEnumerable<byte> res = new byte[0];
            for (int i = 0; i < len / blocksize; i++)
            {
                res = res.Concat(Decrypt(buf.Skip(i * blocksize).Take(blocksize).ToArray()));
            }
            int newlen = BitConverter.ToInt32(res.ToArray(), 0);
            return res.Skip(4).Take(newlen).ToArray();
        }

        public void Decode(BinaryReader re, BinaryWriter wr, int len)
        {
            int blocksize = Nb * Nb;
            if (len == 0)
                return;

            if (len % blocksize != 0)
                return;

            byte[] tmp = new byte[blocksize];
            Array.Clear(tmp, 0, blocksize);
            re.Read(tmp, 0, blocksize);
            byte[] res = Decrypt(tmp);
            int newlen = BitConverter.ToInt32(res, 0);
            if (newlen > blocksize)
            {
                wr.Write(res, 4, blocksize - 4);
            }
            else
            {
                wr.Write(res, 4, newlen);
            }
            newlen -= blocksize - 4;
            while (newlen > blocksize)
            {
                re.Read(tmp, 0, blocksize);
                wr.Write(Decrypt(tmp));
                newlen -= blocksize;
            }
            if (newlen > 0)
            {
                re.Read(tmp, 0, blocksize);
                wr.Write(Decrypt(tmp), 0, newlen);
            }
        }

        public void Decode(Stream src, Stream dst)
        {
            using (var re = new BinaryReader(src))
            using (var wr = new BinaryWriter(dst))
                Decode(re, wr, (int)src.Length);
        }

        private byte[] Decrypt(byte[] data)
        {

            int j = 0;
            byte[,] state = new byte[4, 4];
            for (int i = 0; i < 4; i++)
            {
                for (int k = 0; k < Nb; k++)
                {
                    state[i, k] = data[j++];
                }
            }

            AddRoundKey(state, expKey.Skip(Nr * Nb).Take(Nb));

            for (var i = Nr - 1; i > 0; --i)
            {
                InvShiftRows(state);
                InvSubBytes(state);
                AddRoundKey(state, expKey.Skip(i * Nb).Take(Nb));
                InvMixColumns(state);
            }

            InvShiftRows(state);
            InvSubBytes(state);
            AddRoundKey(state, expKey.Take(Nb));

            j = 0;

            for (int i = 0; i < 4; i++)
            {
                for (int k = 0; k < Nb; k++)
                {
                    data[j++] = state[i, k];
                }
            }

            return data;
        }

        private byte[][] KeyExpansion(byte[] key)
        {

            byte[] temp = new byte[4];
            byte[][] w = new byte[Nb * (Nr + 1)][];
            for (int j = 0; j < Nb * (Nr + 1); j++)
            {
                w[j] = new byte[4];
            }

            for (int j = 0; j < Nk; j++)
                for (int k = 0; k < 4; k++)
                    w[j][k] = key[4 * j + k];

            int i = Nk;

            while (i < Nb * (Nr + 1))
            {
                w[i - 1].CopyTo(temp, 0);
                if ((i % Nk) == 0)
                {
                    RotWord(temp);
                    SubWord(temp);
                    for (int j = 0; j < 4; j++)
                    {
                        temp[j] ^= Rcon[j / Nk][j];
                    }
                }
                else if ((Nk > 6) && ((i % Nk) == 4))
                    SubWord(temp);
                for (int j = 0; j < 4; j++)
                {
                    w[i][j] = (byte)(w[i - Nk][j] ^ temp[j]);
                }
                ++i;
            }
            return w;
        }

        private void RotWord(byte[] temp)
        {
            byte t = temp[0];
            for (int i = 0; i < temp.Length - 1; i++)
                temp[i] = temp[i + 1];
            temp[temp.Length - 1] = t;
        }

        private void SubWord(byte[] Word)
        {
            for (int i = 0; i < Word.Length; i++)
                Word[i] = Sbox[Word[i]];
        }

        private byte[] Encrypt(byte[] data)
        {
            int j = 0;
            byte[,] state = new byte[4, 4];
            for (int i = 0; i < 4; i++)
            {
                for (int k = 0; k < Nb; k++)
                {
                    state[i, k] = data[j++];
                }
            }


            AddRoundKey( state, expKey.Take(Nb));

            for (var i = 1; i <= Nr - 1; i++)
            {
                SubBytes(state);
                ShiftRows(state);
                MixColumns(state);
                AddRoundKey( state, expKey.Skip(i * Nb).Take(Nb));
            }

            SubBytes(state);
            ShiftRows(state);
            AddRoundKey(state, expKey.Skip(Nr * Nb).Take(Nb));

            j = 0;

            for (int i = 0; i < 4; i++)
            {
                for (int k = 0; k < Nb; k++)
                    data[j++] = state[i, k];
            }

            return data;
        }

        private void AddRoundKey( byte[,] state, IEnumerable<byte[]> enumerable)
        {
            int i = 0;
            foreach (byte[] word in enumerable)
            {
                for (int j = 0; j < word.Length; j++)
                {
                    state[i, j] ^= word[j];
                }
                ++i;
            }
        }

        private void ShiftRows( byte[,] state)
        {
            for (int i = 0; i < Nb; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    byte t = state[i, 0];
                    for (int k = 0; k < Nb - 1; k++)
                        state[i, k] = state[i, k + 1];
                    state[i, Nb - 1] = t;
                }
            }
        }

        private void MixColumns( byte[,] s)
        {
            byte[,] state = new byte[Nb, Nb];
            for (int c = 0; c < 4; c++)
            {
                state[0, c] = (Byte)(GMul(0x02, s[0, c]) ^ GMul(0x03, s[1, c]) ^ s[2, c] ^ s[3, c]);
                state[1, c] = (Byte)(s[0, c] ^ GMul(0x02, s[1, c]) ^ GMul(0x03, s[2, c]) ^ s[3, c]);
                state[2, c] = (Byte)(s[0, c] ^ s[1, c] ^ GMul(0x02, s[2, c]) ^ GMul(0x03, s[3, c]));
                state[3, c] = (Byte)(GMul(0x03, s[0, c]) ^ s[1, c] ^ s[2, c] ^ GMul(0x02, s[3, c]));
            }
            for (int i = 0; i < Nb; i++)
            {
                for (int j = 0; j < Nb; j++)
                {
                    s[i, j] = state[i, j];
                }
            }
        }

        private Byte GMul(Byte a, Byte b)
        {
            Byte p = 0;
            Byte counter;
            Byte hi_bit_set;
            for (counter = 0; counter < 8; counter++)
            {
                if ((b & 1) != 0)
                {
                    p ^= a;
                }
                hi_bit_set = (Byte)(a & 0x80);
                a <<= 1;
                if (hi_bit_set != 0)
                {
                    a ^= 0x1b; /* x^8 + x^4 + x^3 + x + 1 */
                }
                b >>= 1;
            }
            return p;
        }

        private void SubBytes( byte[,] state)
        {
            for (int i = 0; i < Nb; i++)
            {
                for (int j = 0; j < Nb; j++)
                {
                    state[i, j] = Sbox[state[i, j]];
                }
            }
        }

        private void InvMixColumns(byte[,] s)
        {
            byte[,] state = new byte[Nb, Nb];
            for (int c = 0; c < 4; c++)
            {
                state[0, c] = (Byte)(GMul(14, s[0, c]) ^ GMul(11, s[1, c]) ^ GMul(13, s[2, c]) ^ GMul(9, s[3, c]));
                state[1, c] = (Byte)(GMul(9, s[0, c]) ^ GMul(14, s[1, c]) ^ GMul(11, s[2, c]) ^ GMul(13, s[3, c]));
                state[2, c] = (Byte)(GMul(13, s[0, c]) ^ GMul(9, s[1, c]) ^ GMul(14, s[2, c]) ^ GMul(11, s[3, c]));
                state[3, c] = (Byte)(GMul(11, s[0, c]) ^ GMul(13, s[1, c]) ^ GMul(9, s[2, c]) ^ GMul(14, s[3, c]));
            }
            for (int i = 0; i < Nb; i++)
            {
                for (int j = 0; j < Nb; j++)
                {
                    s[i, j] = state[i, j];
                }
            }
        }

        private void InvSubBytes(byte[,] state)
        {
            for (int i = 0; i < Nb; i++)
            {
                for (int j = 0; j < Nb; j++)
                {
                    state[i, j] = InvSbox[state[i, j]];
                }
            }
        }

        private void InvShiftRows(byte[,] state)
        {
            for (int i = 0; i < Nb; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    byte t = state[i, Nb - 1];
                    for (int k = Nb - 1; k > 0; --k)
                    {
                        state[i, k] = state[i, k - 1];
                    }
                    state[i, 0] = t;
                }
            }
        }
    }
}
