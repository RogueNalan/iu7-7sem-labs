﻿using Encrypter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab3
{
    public partial class lab3 : Form
    {
        private readonly Aes encrypter;

        public lab3()
        {
            InitializeComponent();
            byte[] key = new byte[16];
            new Random().NextBytes(key);
            encrypter = new Aes(key);
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofd.ShowDialog() == DialogResult.OK && sfd.ShowDialog() == DialogResult.OK)
                {
                    using (var src = new FileStream(ofd.FileName, FileMode.Open))
                    using (var dst = new FileStream(sfd.FileName, FileMode.Create))
                        encrypter.Encode(src, dst);

                    MessageBox.Show("Операция успешно завершена!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                ofd.FileName = "";
                sfd.FileName = "";
            }
        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofd.ShowDialog() == DialogResult.OK && sfd.ShowDialog() == DialogResult.OK)
                {
                    using (var src = new FileStream(ofd.FileName, FileMode.Open))
                    using (var dst = new FileStream(sfd.FileName, FileMode.Create))
                        encrypter.Decode(src, dst);

                    MessageBox.Show("Операция успешно завершена!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                ofd.FileName = "";
                sfd.FileName = "";
            }
        }
    }
}
