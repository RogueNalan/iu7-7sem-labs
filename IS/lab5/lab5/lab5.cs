﻿using Signer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab5
{
    public partial class lab5 : Form
    {
        private SignManager manager = new SignManager("SHA1");

        public lab5()
        {
            InitializeComponent();
        }

        private void btnSign_Click(object sender, EventArgs e)
        {
            try
            {
                var ofd = new OpenFileDialog();
                var sfd = new SaveFileDialog();

                if (ofd.ShowDialog() == DialogResult.OK && sfd.ShowDialog() == DialogResult.OK)
                {
                    using (var ofs = new FileStream(ofd.FileName, FileMode.Open))
                    using (var sfs = new FileStream(sfd.FileName, FileMode.Create))
                        manager.Sign(ofs, sfs);
                }
                MessageBox.Show("Операция выполнена успешно!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCheckSign_Click(object sender, EventArgs e)
        {
            try
            {
                var ofdFile = new OpenFileDialog();
                var ofdSign = new OpenFileDialog();

                if (ofdFile.ShowDialog() == DialogResult.OK && ofdSign.ShowDialog() == DialogResult.OK)
                {
                    bool res;
                    using (var file = new FileStream(ofdFile.FileName, FileMode.Open))
                    using (var sign = new FileStream(ofdSign.FileName, FileMode.Open))
                        res = manager.CheckSign(file, sign);
                    if (res)
                        MessageBox.Show("Подпись верна!");
                    else
                        MessageBox.Show("Подпись неверна!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
