﻿namespace lab5
{
    partial class lab5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            manager.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSign = new System.Windows.Forms.Button();
            this.btnCheckSign = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSign
            // 
            this.btnSign.Location = new System.Drawing.Point(13, 13);
            this.btnSign.Name = "btnSign";
            this.btnSign.Size = new System.Drawing.Size(75, 23);
            this.btnSign.TabIndex = 0;
            this.btnSign.Text = "Подписать";
            this.btnSign.UseVisualStyleBackColor = true;
            this.btnSign.Click += new System.EventHandler(this.btnSign_Click);
            // 
            // btnCheckSign
            // 
            this.btnCheckSign.Location = new System.Drawing.Point(95, 12);
            this.btnCheckSign.Name = "btnCheckSign";
            this.btnCheckSign.Size = new System.Drawing.Size(127, 23);
            this.btnCheckSign.TabIndex = 1;
            this.btnCheckSign.Text = "Проверить подпись";
            this.btnCheckSign.UseVisualStyleBackColor = true;
            this.btnCheckSign.Click += new System.EventHandler(this.btnCheckSign_Click);
            // 
            // lab5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(242, 55);
            this.Controls.Add(this.btnCheckSign);
            this.Controls.Add(this.btnSign);
            this.Name = "lab5";
            this.Text = "lab5";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSign;
        private System.Windows.Forms.Button btnCheckSign;
    }
}

