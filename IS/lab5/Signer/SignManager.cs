﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Signer
{
    public sealed class SignManager : IDisposable
    {
        private readonly HashAlgorithm hashAlgorithm;

        private readonly DSACryptoServiceProvider provider;

        private readonly int cspBlobLength;

        private readonly DSASignatureFormatter formatter;

        private readonly DSASignatureDeformatter deformatter;

        private bool disposed = false;

        private const int dsaSize = 40;

        public SignManager(string hashAlgorithmName)
        {
            this.hashAlgorithm = HashAlgorithm.Create(hashAlgorithmName);
            this.provider = new DSACryptoServiceProvider();
            var blob = provider.ExportCspBlob(false);
            cspBlobLength = blob.Length;

            this.formatter = new DSASignatureFormatter(provider);
            this.deformatter = new DSASignatureDeformatter(provider);
            formatter.SetHashAlgorithm(hashAlgorithmName);
            deformatter.SetHashAlgorithm(hashAlgorithmName);
        }

        public void Sign(Stream src, Stream dst)
        {
            if (disposed)
                throw new ObjectDisposedException("SignManager");

            using (var writer = new BinaryWriter(dst))
            {
                var cspBlob = provider.ExportCspBlob(false);
                writer.Write(cspBlob);
                var hash = hashAlgorithm.ComputeHash(src);
                var sign = formatter.CreateSignature(hash);
                writer.Write(sign);
            }
        }

        public bool CheckSign(Stream src, Stream signStream)
        {
            if (disposed)
                throw new ObjectDisposedException("SignManager");

            using (var signReader = new BinaryReader(signStream))
            {
                var readBlob = signReader.ReadBytes(cspBlobLength);
                provider.ImportCspBlob(readBlob);
                var hash = hashAlgorithm.ComputeHash(src);
                var sign = signReader.ReadBytes(dsaSize);
                return deformatter.VerifySignature(hash, sign);
            }
        }

        public void Dispose()
        {
            hashAlgorithm.Dispose();
            provider.Dispose();

            disposed = true;
        }
    }
}
