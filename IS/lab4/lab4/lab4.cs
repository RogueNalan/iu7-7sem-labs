﻿using Encrypter;
using Encrypter.PrimeGenerators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab4
{
    public partial class lab4 : Form
    {
        private readonly Task<Rsa> rsa = Task.Run(() =>
        {
            Rsa res = null;
            bool success;
            do
            { 
                success = true;
                try
                {
                    var numbers = new MillerRabin().GetNumber(64, 2);
                    res = Rsa.Create(numbers[0], numbers[1]);
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            while (!success);
            return res;
        });

        public lab4()
        {
            InitializeComponent();
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofd.ShowDialog() == DialogResult.OK && sfd.ShowDialog() == DialogResult.OK)
                {
                    var rsaRes = rsa.Result;
                    using (var src = new FileStream(ofd.FileName, FileMode.Open))
                    using (var dst = new FileStream(sfd.FileName, FileMode.Create))
                        rsaRes.Encrypt(src, dst);

                    MessageBox.Show("Операция успешно завершена!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                ofd.FileName = "";
                sfd.FileName = "";
            }
        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofd.ShowDialog() == DialogResult.OK && sfd.ShowDialog() == DialogResult.OK)
                {
                    var rsaRes = rsa.Result;
                    using (var src = new FileStream(ofd.FileName, FileMode.Open))
                    using (var dst = new FileStream(sfd.FileName, FileMode.Create))
                        rsaRes.Decrypt(src, dst);

                    MessageBox.Show("Операция успешно завершена!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                ofd.FileName = "";
                sfd.FileName = "";
            }
        }
    }
}
