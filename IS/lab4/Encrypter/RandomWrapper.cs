﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Encrypter
{
    static class RandomWrapper
    {
        private static Random random = new Random();

        private static object monitor = new object();

        public static int Next()
        {
            lock (monitor)
                return random.Next();
        }

        public static int Next(int maxValue)
        {
            lock (monitor)
                return random.Next(maxValue);
        }

        public static int Next(int minValue, int maxValue)
        {
            lock (monitor)
                return random.Next(minValue, maxValue);
        }

        public static void NextBytes(byte[] buffer)
        {
            lock (monitor)
                random.NextBytes(buffer);
        }
    }
}
