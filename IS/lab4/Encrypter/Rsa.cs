﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Encrypter
{
    public sealed class Rsa
    {
        private readonly BigInteger n;

        private readonly BigInteger d;

        private readonly int e;

        private readonly int blockLength;

        public BigInteger Encrypt(BigInteger message)
        {
            if (message >= n)
                throw new ArgumentOutOfRangeException("message");

            return BigInteger.ModPow(message, e, n);
        }

        public BigInteger Decrypt(BigInteger cryptedMessage)
        {
            if (cryptedMessage >= n)
                throw new ArgumentOutOfRangeException("cryptedMessage");

            return BigInteger.ModPow(cryptedMessage, d, n);
        }

        public byte[] Encrypt(byte[] message)
        {
            if (message == null)
                throw new ArgumentNullException("message");
            BigInteger msg = new BigInteger(message);
            if (msg >= n)
                throw new ArgumentException("message");
            return Encrypt(msg).ToByteArray();
        }

        public byte[] Decrypt(byte[] cryptedMessage)
        {
            if (cryptedMessage == null)
                throw new ArgumentNullException("message");
            BigInteger cMsg = new BigInteger(cryptedMessage);
            if (cMsg >= n)
                throw new ArgumentException("message");
            return Decrypt(cMsg).ToByteArray();
        }

        public void Encrypt(Stream src, Stream dst)
        {
            byte[] buffer = new byte[blockLength];
            byte[] size = BitConverter.GetBytes(src.Length - src.Position);
            size.CopyTo(buffer, 0);
            int read = src.Read(buffer, size.Length, blockLength - size.Length - 1);
            WriteBlock(dst, Encrypt(buffer), blockLength);
            read = src.Read(buffer, 0, blockLength - 1);
            while (read != 0)
            {
                if (read != blockLength - 1)
                    Array.Clear(buffer, read, blockLength - read);
                WriteBlock(dst, Encrypt(buffer), blockLength);
                read = src.Read(buffer, 0, blockLength - 1);
            }
        }

        public void Decrypt(Stream src, Stream dst)
        {
            byte[] buffer = new byte[blockLength];
            src.Read(buffer, 0, blockLength);

            byte[] decr = Decrypt(buffer);
            if (decr.Length < 8)
                decr = new byte[8];
            long currLength = BitConverter.ToInt64(decr, 0);
            int toWrite = (currLength < blockLength) ? (int)currLength : decr.Length - 8;
            dst.Write(decr, 8, toWrite);
            currLength -= toWrite;

            long fullRounds = currLength / (blockLength - 1);
            long remaining = currLength % (blockLength - 1);
            for (long i = 0; i < fullRounds; ++i)
            {
                src.Read(buffer, 0, blockLength);
                WriteBlock(dst, Decrypt(buffer), blockLength - 1);
            }
            if (currLength != 0)
            {
                Array.Clear(buffer, 0, blockLength);
                src.Read(buffer, 0, blockLength);
                WriteBlock(dst, Decrypt(buffer), (int)remaining);
            }
        }

        private Rsa(int e, BigInteger d, BigInteger n)
        {
            if (e < 2)
                throw new ArgumentOutOfRangeException("e");
            if (d < 2)
                throw new ArgumentOutOfRangeException("d");
            if ((n < 2) || (n <= d) || (n <= e))
                throw new ArgumentOutOfRangeException("n");

            this.e = e;
            this.d = d;
            this.n = n;
            this.blockLength = n.ToByteArray().Length;
        }

        public static Rsa Create(BigInteger p, BigInteger q)
        {
            if (p < 2)
                throw new ArgumentOutOfRangeException("p");
            if (q < 2)
                throw new ArgumentOutOfRangeException("q");

            BigInteger n = p * q;
            BigInteger phi = n - p - q + 1; // (p - 1) * (q - 1) == pq - q - p + 1
            int e = (65537 < phi) ? 65537 : 17;
            var inv = FindInverse(e, phi);
            if (inv.HasValue)
                return new Rsa(e, inv.Value, n);
            else
                throw new ArgumentException();
        }

        private static void WriteBlock(Stream dst, byte[] block, int expectedLength)
        {
            int diff = expectedLength - block.Length;
            if (diff > 0)
            {
                dst.Write(block, 0, block.Length);
                dst.Write(new byte[diff], 0, diff);
            }
            else
                dst.Write(block, 0, expectedLength);
        }

        private static BigInteger? FindInverse(BigInteger a, BigInteger b)
        {
            BigInteger x2 = 1, x1 = 0;
            BigInteger y2 = 0, y1 = 1;
            BigInteger x, y;
            while (b > 0)
            {
                BigInteger q = a / b, r = a - q * b;
                x = x2 - q * x1;
                y = y2 - q * y1;

                a = b;
                b = r;
                x2 = x1;
                x1 = x;
                y2 = y1;
                y1 = y;
            }

            return (a.IsOne) ? (BigInteger?)x2 : null;
        }
    }
}
