﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Encrypter.PrimeGenerators
{
    public sealed class MillerRabin : IPrimeNumberGenerator
    {
        public IList<BigInteger> GetNumber(int bytesCount = 16, int count = 1)
        {
            if (bytesCount < 1)
                throw new ArgumentOutOfRangeException("bytesCount");
            if (count < 1)
                throw new ArgumentOutOfRangeException("count");

            var res = new HashSet<BigInteger>();
            for (int i = 0; i < count; ++i)
            {
                bool success = false;
                int rounds = bytesCount / 2;
                if (rounds < 1)
                    rounds = 1;
                do
                {
                    BigInteger min, max;
                    do
                    {
                        NextBigInt(bytesCount, out min);
                        NextBigInt(bytesCount, out max);
                    }
                    while (min == max);
                    if (min > max)
                    {
                        BigInteger temp = min;
                        min = max;
                        max = temp;
                    }

                    for (BigInteger curr = min; curr <= max; ++curr)
                        if (!res.Contains(curr) && TestPrime(curr, rounds))
                        {
                            res.Add(curr);
                            success = true;
                            break;
                        }
                }
                while (success == false);
            }

            return res.ToArray();
        }

        private static bool TestPrime(BigInteger number, int rounds = 1)
        {
            BigInteger max = number - 2;
            var used = new HashSet<BigInteger>();
            object usedMonitor = new object();
            long size = number.ToByteArray().LongLength;
            bool res = true;
            Parallel.For(0, rounds, (i, state) =>
            {
                BigInteger witness;
                bool success = false;
                do
                {
                    NextBigInt(size, out witness, max);
                    lock (usedMonitor)
                    {
                        if (!used.Contains(witness))
                        {
                            used.Add(witness);
                            success = true;
                        }
                    }
                }
                while (!success);
                if (!TestPrimeIter(number, witness))
                {
                    res = false;
                    state.Stop();
                }
            });
            return res;
        }

        private static bool TestPrimeIter(BigInteger number, BigInteger witness)
        {
            int s = 0;
            BigInteger minNumber = number - 1;
            BigInteger d = minNumber;
            while (d.IsEven)
            {
                d /= 2;
                ++s;
            }

            BigInteger x = BigInteger.ModPow(witness, d, number);
            if (x.IsOne || x == minNumber)
                return true;
            for (int i = 1; i < s; ++i)
            {
                x = BigInteger.ModPow(x, 2, number);
                if (x.IsOne)
                    return false;
                if (x == minNumber)
                    break;
            }
            return x == minNumber;
        }

        private static void NextBigInt(long size, out BigInteger res, BigInteger? max = null)
        {
            byte[] arr = new byte[size];
            do
            {
                RandomWrapper.NextBytes(arr);
                res = BigInteger.Abs(new BigInteger(arr));
            }
            while (res < 2 || (max.HasValue && res >= max));
        }
    }
}
