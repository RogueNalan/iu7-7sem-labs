﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Encrypter.PrimeGenerators
{
    public interface IPrimeNumberGenerator
    {
        IList<BigInteger> GetNumber(int bytesCount, int count);
    }
}
