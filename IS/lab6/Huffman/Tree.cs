﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Huffman
{
    public sealed class Tree
    {
        private readonly Node head;

        private readonly TreeReader reader;

        public byte[] Encode(byte[] src)
        {
            byte[] tree = Serialize().SelectMany(x => BitConverter.GetBytes(x)).ToArray();
            byte[] length = BitConverter.GetBytes(src.Length);
            BitArray data = new BitArray(src.SelectMany(x => Traverse(x).Cast<bool>()).ToArray());

            int resLength = tree.Length + length.Length + (data.Length / 8);
            if (data.Length % 8 != 0)
                ++resLength;

            byte[] res = new byte[resLength];
            tree.CopyTo(res, 0);
            length.CopyTo(res, tree.Length);
            data.CopyTo(res, tree.Length + length.Length);
            return res;
        }

        public byte[] Decode(byte[] src)
        {
            return reader.Decode(src);
        }

        public byte[] Decode(Stream src)
        {
            return reader.Decode(src);
        }

        public static Tree Create(byte[] contents)
        {
            if (contents.Length == 0)
                return new Tree(new Node(new Node(0, 0), new Node(1, 0), 0));

            var counted = contents.GroupBy(x => x)
                .Select(x => new Node(x.Key, x.Count()))
                .ToList();
            
            while (counted.Count != 1)
            {
                int minInd = MinIndex(counted);
                var val1 = counted[minInd];
                counted.RemoveAt(minInd);

                minInd = MinIndex(counted);
                var val2 = counted[minInd];
                counted.RemoveAt(minInd);

                counted.Add(new Node(val1, val2, val1.Count + val2.Count));
            }
            return new Tree(counted[0]);
        }

        public static Tree Load(Stream src)
        {
            return new Tree(ReadNode(src));
        }

        private Tree(Node head)
        {
            if (head == null)
                throw new ArgumentNullException("head");
            this.head = head;
            this.reader = new TreeReader(head);
        }

        private IList<short> Serialize()
        {
            return SerializeInner(new List<short>(), head);
        }

        private List<short> SerializeInner(List<short> res, Node current)
        {
            if (current.Value.HasValue)
            {
                res.Add(current.Value.Value);
                return res;
            }
            else
                res.Add(-1);

            if (current.Left != null)
                SerializeInner(res, current.Left);
            if (current.Right != null)
                SerializeInner(res, current.Right);
            return res;
        }

        private List<bool> Traverse(byte value)
        {
            return TraverseInner(value, new List<bool>(), head);
        }

        private List<bool> TraverseInner(byte value, List<bool> path, Node current)
        {
            if (current.Value == value)
                return path;
            if (current.Left != null)
            {
                path.Add(true);
                List<bool> tempRes = TraverseInner(value, path, current.Left);
                if (tempRes != null)
                    return path;
                path[path.Count - 1] = false;
                tempRes = TraverseInner(value, path, current.Right);
                if (tempRes != null)
                    return path;
                path.RemoveAt(path.Count - 1);
            }
            return null;
        }

        private static Node ReadNode(Stream src)
        {
            byte[] buffer = new byte[2];
            src.Read(buffer, 0, 2);
            short val = BitConverter.ToInt16(buffer, 0);
            if (val != -1)
                return new Node((byte)val, 0);
            else
                return new Node(ReadNode(src), ReadNode(src), 0);
        }

        private static int MinIndex(IList<Node> values)
        {
            int minCount = values[0].Count;
            int minInd = 0;

            for (int i = 1; i < values.Count; ++i)
                if (values[i].Count < minCount)
                {
                    minCount = values[i].Count;
                    minInd = i;
                }
            return minInd;
        }
    }
}
