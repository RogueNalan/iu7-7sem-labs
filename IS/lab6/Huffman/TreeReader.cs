﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Huffman
{
    sealed class TreeReader
    {
        private readonly Node head;

        public TreeReader(Node head)
        {
            if (head == null)
                throw new ArgumentNullException("tree");
            this.head = head;
        }

        public byte[] Decode(byte[] src)
        {
            if (src == null)
                throw new ArgumentNullException();
            if (src.Length < 4)
                throw new ArgumentException("src");

            int fileLength = BitConverter.ToInt32(src, 0);
            BitArray array = new BitArray(src.Skip(4).ToArray());
            if (fileLength == 0)
                return new byte[0];
            if (array.Length == 0)
                return Enumerable.Repeat(head.Value.Value, fileLength).ToArray();

            byte[] res = new byte[fileLength];
            int count = 0;
            int i = 0;
            Node curr = head;
            while (count < fileLength)
            {
                if (curr.Value.HasValue)
                {
                    res[count++] = curr.Value.Value;
                    curr = head;
                }
                if (array[i++])
                    curr = curr.Left;
                else
                    curr = curr.Right;
            }
            return res;
        }

        public byte[] Decode(Stream src)
        {
            int length = (int)(src.Length - src.Position);
            byte[] buffer = new byte[length];
            src.Read(buffer, 0, length);
            return Decode(buffer);
        }
    }
}
