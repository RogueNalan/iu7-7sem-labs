﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Huffman
{
    sealed class Node
    {
        public Node Left { get; } = null;

        public Node Right { get; } = null;

        public int Count { get; } = 0;

        public byte? Value { get; } = null;

        public Node(byte value, int count)
        {
            Count = count;
            Value = value;
        }

        public Node(Node left, Node right, int count)
        {
            Left = left;
            Right = right;
            Count = count;
        }
    }
}
