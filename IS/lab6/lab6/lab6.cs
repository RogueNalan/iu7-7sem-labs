﻿using Huffman;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab6
{
    public partial class lab6 : Form
    {
        public lab6()
        {
            InitializeComponent();
        }

        private void btnArchive_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofd.ShowDialog() == DialogResult.OK && sfd.ShowDialog() == DialogResult.OK)
                {
                    var src = File.ReadAllBytes(ofd.FileName);
                    var res = Tree.Create(src).Encode(src);
                    File.WriteAllBytes(sfd.FileName, res);

                    MessageBox.Show("Операция успешно завершена!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                ofd.FileName = "";
                sfd.FileName = "";
            }
        }

        private void btnUnarchive_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofd.ShowDialog() == DialogResult.OK && sfd.ShowDialog() == DialogResult.OK)
                {
                    using (var src = new FileStream(ofd.FileName, FileMode.Open))
                        File.WriteAllBytes(sfd.FileName, Tree.Load(src).Decode(src));

                    MessageBox.Show("Операция успешно завершена!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                ofd.FileName = "";
                sfd.FileName = "";
            }
        }
    }
}
