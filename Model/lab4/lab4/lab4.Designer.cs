﻿namespace lab4
{
    partial class lab4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbLambda = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbInterval = new System.Windows.Forms.TextBox();
            this.btnPoisson = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbFrom = new System.Windows.Forms.TextBox();
            this.tbTo = new System.Windows.Forms.TextBox();
            this.btnUniform = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbMaxSize = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbDeltaT = new System.Windows.Forms.TextBox();
            this.tbReturnProbability = new System.Windows.Forms.TextBox();
            this.btnQueue = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.lbPassed = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbDropped = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbSize = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbRecycled = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbCurrTime = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lbAverageSize = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tbFrameInterval = new System.Windows.Forms.TextBox();
            this.btnFrameInterval = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "События на вход (распределение Пуассона):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Лямбда:";
            // 
            // tbLambda
            // 
            this.tbLambda.Location = new System.Drawing.Point(85, 39);
            this.tbLambda.Name = "tbLambda";
            this.tbLambda.Size = new System.Drawing.Size(100, 20);
            this.tbLambda.TabIndex = 2;
            this.tbLambda.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Интервал:";
            // 
            // tbInterval
            // 
            this.tbInterval.Location = new System.Drawing.Point(85, 72);
            this.tbInterval.Name = "tbInterval";
            this.tbInterval.Size = new System.Drawing.Size(100, 20);
            this.tbInterval.TabIndex = 4;
            this.tbInterval.Text = "1";
            // 
            // btnPoisson
            // 
            this.btnPoisson.Location = new System.Drawing.Point(22, 98);
            this.btnPoisson.Name = "btnPoisson";
            this.btnPoisson.Size = new System.Drawing.Size(225, 23);
            this.btnPoisson.TabIndex = 5;
            this.btnPoisson.Text = "Задать";
            this.btnPoisson.UseVisualStyleBackColor = true;
            this.btnPoisson.Click += new System.EventHandler(this.btnPoisson_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(284, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(261, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "События на выход (равномерное распределение):";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(287, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "От:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(287, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "До:";
            // 
            // tbFrom
            // 
            this.tbFrom.Location = new System.Drawing.Point(317, 39);
            this.tbFrom.Name = "tbFrom";
            this.tbFrom.Size = new System.Drawing.Size(100, 20);
            this.tbFrom.TabIndex = 9;
            this.tbFrom.Text = "0";
            // 
            // tbTo
            // 
            this.tbTo.Location = new System.Drawing.Point(317, 71);
            this.tbTo.Name = "tbTo";
            this.tbTo.Size = new System.Drawing.Size(100, 20);
            this.tbTo.TabIndex = 10;
            this.tbTo.Text = "1";
            // 
            // btnUniform
            // 
            this.btnUniform.Location = new System.Drawing.Point(287, 97);
            this.btnUniform.Name = "btnUniform";
            this.btnUniform.Size = new System.Drawing.Size(258, 23);
            this.btnUniform.TabIndex = 11;
            this.btnUniform.Text = "Задать";
            this.btnUniform.UseVisualStyleBackColor = true;
            this.btnUniform.Click += new System.EventHandler(this.btnUniform_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(653, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Параметры очередеи:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(588, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(130, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Максимальный размер:";
            // 
            // tbMaxSize
            // 
            this.tbMaxSize.Location = new System.Drawing.Point(725, 39);
            this.tbMaxSize.Name = "tbMaxSize";
            this.tbMaxSize.Size = new System.Drawing.Size(100, 20);
            this.tbMaxSize.TabIndex = 14;
            this.tbMaxSize.Text = "10";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(588, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(115, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Изменение времени:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(588, 98);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Шанс возврата:";
            // 
            // tbDeltaT
            // 
            this.tbDeltaT.Location = new System.Drawing.Point(725, 66);
            this.tbDeltaT.Name = "tbDeltaT";
            this.tbDeltaT.Size = new System.Drawing.Size(100, 20);
            this.tbDeltaT.TabIndex = 17;
            this.tbDeltaT.Text = "0.01";
            // 
            // tbReturnProbability
            // 
            this.tbReturnProbability.Location = new System.Drawing.Point(725, 98);
            this.tbReturnProbability.Name = "tbReturnProbability";
            this.tbReturnProbability.Size = new System.Drawing.Size(100, 20);
            this.tbReturnProbability.TabIndex = 18;
            this.tbReturnProbability.Text = "0.1";
            // 
            // btnQueue
            // 
            this.btnQueue.Location = new System.Drawing.Point(588, 132);
            this.btnQueue.Name = "btnQueue";
            this.btnQueue.Size = new System.Drawing.Size(237, 23);
            this.btnQueue.TabIndex = 19;
            this.btnQueue.Text = "Задать";
            this.btnQueue.UseVisualStyleBackColor = true;
            this.btnQueue.Click += new System.EventHandler(this.btnQueue_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(22, 204);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(225, 23);
            this.btnStart.TabIndex = 20;
            this.btnStart.Text = "Начать моделирование";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(287, 204);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(258, 23);
            this.btnStop.TabIndex = 21;
            this.btnStop.Text = "Остановить моделирование";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(391, 244);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Статистика:";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(591, 204);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(237, 23);
            this.btnClear.TabIndex = 23;
            this.btnClear.Text = "Удалить модель";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 279);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Заявок обработано:";
            // 
            // lbPassed
            // 
            this.lbPassed.AutoSize = true;
            this.lbPassed.Location = new System.Drawing.Point(180, 279);
            this.lbPassed.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbPassed.Name = "lbPassed";
            this.lbPassed.Size = new System.Drawing.Size(0, 13);
            this.lbPassed.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 305);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Заявок пропущено:";
            // 
            // lbDropped
            // 
            this.lbDropped.AutoSize = true;
            this.lbDropped.Location = new System.Drawing.Point(180, 305);
            this.lbDropped.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbDropped.Name = "lbDropped";
            this.lbDropped.Size = new System.Drawing.Size(0, 13);
            this.lbDropped.TabIndex = 27;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 331);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(140, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Текущий размер очереди:";
            // 
            // lbSize
            // 
            this.lbSize.AutoSize = true;
            this.lbSize.Location = new System.Drawing.Point(180, 331);
            this.lbSize.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbSize.Name = "lbSize";
            this.lbSize.Size = new System.Drawing.Size(0, 13);
            this.lbSize.TabIndex = 29;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(525, 279);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(156, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "Заявок вернулось в очередь:";
            // 
            // lbRecycled
            // 
            this.lbRecycled.AutoSize = true;
            this.lbRecycled.Location = new System.Drawing.Point(690, 279);
            this.lbRecycled.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbRecycled.Name = "lbRecycled";
            this.lbRecycled.Size = new System.Drawing.Size(0, 13);
            this.lbRecycled.TabIndex = 31;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(524, 305);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Времени прошло:";
            // 
            // lbCurrTime
            // 
            this.lbCurrTime.AutoSize = true;
            this.lbCurrTime.Location = new System.Drawing.Point(690, 305);
            this.lbCurrTime.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbCurrTime.Name = "lbCurrTime";
            this.lbCurrTime.Size = new System.Drawing.Size(0, 13);
            this.lbCurrTime.TabIndex = 33;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(525, 331);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(138, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Средний размер очереди:";
            // 
            // lbAverageSize
            // 
            this.lbAverageSize.AutoSize = true;
            this.lbAverageSize.Location = new System.Drawing.Point(690, 331);
            this.lbAverageSize.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbAverageSize.Name = "lbAverageSize";
            this.lbAverageSize.Size = new System.Drawing.Size(0, 13);
            this.lbAverageSize.TabIndex = 35;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(22, 175);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(129, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "Интервал отображения:";
            // 
            // tbFrameInterval
            // 
            this.tbFrameInterval.Location = new System.Drawing.Point(158, 175);
            this.tbFrameInterval.Name = "tbFrameInterval";
            this.tbFrameInterval.Size = new System.Drawing.Size(100, 20);
            this.tbFrameInterval.TabIndex = 37;
            this.tbFrameInterval.Text = "500000";
            // 
            // btnFrameInterval
            // 
            this.btnFrameInterval.Location = new System.Drawing.Point(265, 175);
            this.btnFrameInterval.Name = "btnFrameInterval";
            this.btnFrameInterval.Size = new System.Drawing.Size(75, 23);
            this.btnFrameInterval.TabIndex = 38;
            this.btnFrameInterval.Text = "Задать";
            this.btnFrameInterval.UseVisualStyleBackColor = true;
            this.btnFrameInterval.Click += new System.EventHandler(this.btnFrameInterval_Click);
            // 
            // lab4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 364);
            this.Controls.Add(this.btnFrameInterval);
            this.Controls.Add(this.tbFrameInterval);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.lbAverageSize);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.lbCurrTime);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lbRecycled);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.lbSize);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lbDropped);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lbPassed);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnQueue);
            this.Controls.Add(this.tbReturnProbability);
            this.Controls.Add(this.tbDeltaT);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbMaxSize);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnUniform);
            this.Controls.Add(this.tbTo);
            this.Controls.Add(this.tbFrom);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnPoisson);
            this.Controls.Add(this.tbInterval);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbLambda);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "lab4";
            this.Text = "lab4";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbLambda;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbInterval;
        private System.Windows.Forms.Button btnPoisson;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbFrom;
        private System.Windows.Forms.TextBox tbTo;
        private System.Windows.Forms.Button btnUniform;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbMaxSize;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbDeltaT;
        private System.Windows.Forms.TextBox tbReturnProbability;
        private System.Windows.Forms.Button btnQueue;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbPassed;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbDropped;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbSize;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbRecycled;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbCurrTime;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbAverageSize;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbFrameInterval;
        private System.Windows.Forms.Button btnFrameInterval;
    }
}

