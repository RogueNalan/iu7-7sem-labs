﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueAnalyzer
{
    public class QueueParams
    {
        public int MaxSize { get; set; }

        public double Interval { get; set; }

        public double ReturnProbability { get; set; }

        public QueueParams(int maxSize, double interval, double returnProbability = 0)
        {
            if (maxSize <= 0)
                throw new ArgumentOutOfRangeException("maxSize");
            if (interval < 0 || interval.Compare(0))
                throw new ArgumentOutOfRangeException("interval");
            if (returnProbability < 0)
                throw new ArgumentOutOfRangeException("returnProbability");

            this.MaxSize = maxSize;
            this.Interval = interval;
            this.ReturnProbability = returnProbability;
        }
    }
}
