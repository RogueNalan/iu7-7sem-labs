﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueAnalyzer.EventGenerators
{
    public class UniformEventGenerator : EventGenerator
    {
        private readonly double min;

        private readonly double max;

        public UniformEventGenerator(double min, double max)
        {
            if (min < 0)
                throw new ArgumentOutOfRangeException("min");
            if (max < 0)
                throw new ArgumentOutOfRangeException("max");
            double diff = max - min;
            if (diff < 0 || diff.Compare(0))
                throw new ArgumentOutOfRangeException();

            this.min = min;
            this.max = max;
        }

        public override double NextEvent(double currTime)
        {
            return currTime + Next(min, max);
        }
    }
}
