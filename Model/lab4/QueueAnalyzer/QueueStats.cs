﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueAnalyzer
{
    public class QueueStats
    {
        public long Passed { get; }

        public long Size { get; }

        public long Dropped { get; }

        public long Recycled { get; }

        public double CurrTime { get; }

        public double? AverageSize { get; }

        internal QueueStats(long passed, long size, long dropped, long recycled, double currTime, double? averageSize)
        {
            Passed = passed;
            Size = size;
            Dropped = dropped;
            Recycled = recycled;
            CurrTime = currTime;
            AverageSize = averageSize;
        }
    }
}
