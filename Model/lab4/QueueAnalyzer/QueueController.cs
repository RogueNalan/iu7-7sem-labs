﻿using Common;
using QueueAnalyzer.EventGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QueueAnalyzer
{
    public sealed class QueueController : IDisposable
    {
        public EventGenerator Input
        {
            get
            {
                return runner.Input;
            }
            set
            {
                lock (lockObject)
                    runner.Input = value;
            }
        }

        public EventGenerator Output
        {
            get
            {
                return runner.Output;
            }
            set
            {
                lock (lockObject)
                    runner.Output = value;
            }
        }

        public int MaxSize
        {
            get
            {
                return runner.MaxSize;
            }
            set
            {
                lock (lockObject)
                    runner.MaxSize = value;
            }
        }

        public double Interval
        {
            get
            {
                return runner.Interval;
            }
            set
            {
                lock (lockObject)
                    runner.Interval = value;
            }
        }

        public double ReturnProbability
        {
            get
            {
                return runner.ReturnProbability;
            }
            set
            {
                lock (lockObject)
                    runner.ReturnProbability = value;
            }
        }

        public QueueParams Params
        {
            get
            {
                return new QueueParams(runner.MaxSize, runner.Interval, runner.ReturnProbability);
            }
            set
            {
                if (value != null)
                {
                    lock (lockObject)
                    {
                        runner.MaxSize = value.MaxSize;
                        runner.Interval = value.Interval;
                        runner.ReturnProbability = value.ReturnProbability;
                    }
                }
                else
                    throw new ArgumentNullException("Params");
            }
        }

        public bool IsStarted { get { return tokenSource != null; } }

        private readonly IUiWorker uiWorker;

        private readonly EventRunner runner;

        private readonly object lockObject = new object();

        private Task displayTask = null;

        private CancellationTokenSource tokenSource = null;

        private bool disposed = false;

        public QueueController(EventGenerator input, EventGenerator output, QueueParams queueParams, IUiWorker uiWorker)
        {
            if (input == null)
                throw new ArgumentNullException("input");
            if (output == null)
                throw new ArgumentNullException("output");
            if (uiWorker == null)
                throw new ArgumentNullException("worker");
            if (queueParams == null)
                throw new ArgumentNullException("queueParams");

            this.uiWorker = uiWorker;
            runner = new EventRunner(input, output, queueParams.ReturnProbability, queueParams.MaxSize, queueParams.Interval);
        }

        public void Start()
        {
            lock (lockObject)
            {
                if (disposed)
                    throw new ObjectDisposedException("QueueController");

                if (IsStarted)
                    return;

                tokenSource = new CancellationTokenSource();
                var token = tokenSource.Token;
                Task.Factory.StartNew(() =>
                {
                    while (!token.IsCancellationRequested)
                    {
                        lock (lockObject)
                            runner.Step();
                        if (runner.Iterations % uiWorker.DisplayInterval == 0)
                        {
                            var stats = runner.GetStats();
                            displayTask?.Wait();
                            displayTask = uiWorker.DisplayAsync(stats);
                        }
                    }
                }, token, TaskCreationOptions.LongRunning, TaskScheduler.Current);
            }
        }

        public void Stop()
        {
            lock (lockObject)
            {
                if (disposed)
                    throw new ObjectDisposedException("QueueController");
                StopInner();
            }
        }

        private void StopInner()
        {
            if (IsStarted)
            {
                tokenSource.Cancel();
                tokenSource.Dispose();
                tokenSource = null;
            }
        }

        public void Dispose()
        {
            lock(lockObject)
            {
                StopInner();
                displayTask?.Wait();
                disposed = true;
            }
        }
    }
}
