﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueAnalyzer
{
    public interface IUiWorker
    {
        void Display(QueueStats stats);
        Task DisplayAsync(QueueStats stats);

        long DisplayInterval { get; }
    }
}
