﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueAnalyzer.EventGenerators
{
    public class UniformEventGenerator : EventGenerator
    {
        private readonly double min;

        private readonly double max;

        public override double Average
        {
            get {
                return average;
            }
        }
        private readonly double average;

        public UniformEventGenerator(double min, double max)
        {
            if (min < 0)
                throw new ArgumentOutOfRangeException("min");
            if (max < 0)
                throw new ArgumentOutOfRangeException("max");
            double diff = max - min;
            if (diff < 0 || diff.Compare(0))
                throw new ArgumentOutOfRangeException();

            this.min = min;
            this.max = max;
            this.average = (max - min) / 2;
        }

        public override double NextEvent(double currTime)
        {
            return currTime + Next(min, max);
        }
    }
}
