﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Math;

namespace QueueAnalyzer.EventGenerators
{
    public class PoissonEventGenerator : EventGenerator
    {
        private readonly double epsLambda;

        private readonly double lambda;

        private readonly double interval;

        private IEnumerator<double> nextEvent = Enumerable.Empty<double>().GetEnumerator();

        public override double Average
        {
            get
            {
                return lambda;
            }
        }

        public PoissonEventGenerator(double lambda, double interval)
        {
            if (lambda < 0 || lambda.Compare(0))
                throw new ArgumentOutOfRangeException("lambda");
            if (interval < 0 || interval.Compare(0))
                throw new ArgumentOutOfRangeException("interval");
            this.epsLambda = Pow(E, -lambda);
            if (epsLambda.Compare(0))
                throw new ArgumentOutOfRangeException("lambda");


            this.lambda = lambda;
            this.interval = interval;
        }

        public override double NextEvent(double currTime)
        {
            if (!nextEvent.MoveNext())
            {
                RefreshEvents();
                nextEvent.MoveNext();
            }
            return currTime + nextEvent.Current;
        }

        private void RefreshEvents()
        {
            double startInt = 0;
            double endInt = interval;
            int k = GetK();
            while (k == 0)
            {
                startInt += interval;
                endInt += interval;
                k = GetK();
            }
            nextEvent = NextSequence(startInt, endInt, k)
                .OrderBy(x => x)
                .GetEnumerator();
        }

        private int GetK()
        {
            double prob = Next();
            double mult = epsLambda;
            double sum = mult;
            int k = 0;
            while (prob > sum)
            {
                mult *= lambda / ++k;
                sum += mult;
            }
            return k;
        }
    }
}
