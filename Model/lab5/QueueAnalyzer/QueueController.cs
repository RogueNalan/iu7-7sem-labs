﻿using Common;
using QueueAnalyzer.EventGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QueueAnalyzer
{
    public sealed class QueueController : IDisposable
    {
        public IList<EventGenerator> Generators
        {
            get
            {
                return runner.Generators;
            }
            set
            {
                lock (lockObject)
                    runner.Generators = value;
            }
        }

        public int MaxSize
        {
            get
            {
                return runner.MaxSize;
            }
            set
            {
                lock (lockObject)
                    runner.MaxSize = value;
            }
        }
        
        public double Interval
        {
            get
            {
                return runner.Interval;
            }
            set
            {
                lock (lockObject)
                    runner.Interval = value;
            }
        }

        public bool IsStarted { get { return tokenSource != null; } }

        private readonly IUiWorker uiWorker;

        private readonly EventRunner runner;

        private readonly object lockObject = new object();

        private Task displayTask = null;

        private CancellationTokenSource tokenSource = null;

        private bool disposed = false;

        public QueueController(IList<EventGenerator> generators, QueueParams queueParams, IUiWorker uiWorker)
        {
            if (generators == null)
                throw new ArgumentNullException("generators");
            if (generators.Count != EventRunner.GeneratorsLength)
                throw new ArgumentException("generators");
            if (uiWorker == null)
                throw new ArgumentNullException("worker");
            if (queueParams == null)
                throw new ArgumentNullException("queueParams");

            this.uiWorker = uiWorker;
            runner = new EventRunner(generators, queueParams.MaxSize, queueParams.Interval);
        }

        public void Start()
        {
            lock (lockObject)
            {
                if (disposed)
                    throw new ObjectDisposedException("QueueController");

                if (IsStarted)
                    return;

                tokenSource = new CancellationTokenSource();
                var token = tokenSource.Token;
                Task.Factory.StartNew(() =>
                {
                    while (!token.IsCancellationRequested)
                    {
                        lock (lockObject)
                            runner.Step();
                        if (runner.Iterations % uiWorker.DisplayInterval == 0)
                        {
                            var stats = runner.GetStats();
                            displayTask?.Wait();
                            displayTask = uiWorker.DisplayAsync(stats);
                        }
                    }
                }, token, TaskCreationOptions.LongRunning, TaskScheduler.Current)
                .ContinueWith((task) =>
                {
                    tokenSource.Dispose();
                    tokenSource = null;
                });
            }
        }

        public void Stop()
        {
            lock (lockObject)
            {
                if (disposed)
                    throw new ObjectDisposedException("QueueController");
                StopInner();
            }
        }

        private void StopInner()
        {
            if (IsStarted)
            {
                tokenSource.Cancel();
            }
        }

        public void Dispose()
        {
            lock(lockObject)
            {
                StopInner();
                displayTask?.Wait();
                disposed = true;
            }
        }
    }
}
