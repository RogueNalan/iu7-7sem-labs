﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProbabilitiesCalculator
{
    sealed class GaussCalculator
    {
        private readonly IList<IList<double>> equations;

        private readonly IList<double> answers;

        private readonly int size;

        private double[] solution = null;

        private bool isSolved = false;

        public GaussCalculator(IList<IList<double>> equations, IList<double> answers)
        {
            if (equations == null)
                throw new ArgumentNullException("equations");

            if (answers == null)
                throw new ArgumentNullException("answers");

            if (equations.Count != answers.Count)
                throw new ArgumentException("equations, answers");

            for (int i = 0; i < equations.Count; ++i)
                if (equations[i]?.Count != equations.Count)
                    throw new ArgumentException("equations");

            this.equations = equations;
            this.answers = answers;
            this.size = equations.Count;
        }

        public double[] GetSolution()
        {
            if (!isSolved)
            {
                try
                {
                    Solve();
                }
                catch (Exception)
                {
                    solution = null;
                }
                finally
                {
                    isSolved = true;
                }
            }
            return solution;
        }

        private void Solve()
        {
            for (int j = 0; j < size - 1; ++j)
            {
                int i = j;
                for (; i < size && equations[i][j].Compare(0); ++i)
                    ;
                if (equations[i][j].Compare(0))
                    return;
                if (i != j)
                    SwapStrings(i, j);
                for (int k = j + 1; k < size; ++k)
                    EliminateRow(j, k);
            }

            double[] res = new double[size];
            for (int i = size - 1; i >= 0; --i)
            {
                double sum = 0;
                for (int j = i + 1; j < size; ++j)
                    sum += equations[i][j] * res[j];
                res[i] = (answers[i] - sum) / equations[i][i];
            }
            this.solution = res;
        }

        private void SwapStrings(int i, int j)
        {
            var temp = equations[j];
            equations[j] = equations[i];
            equations[i] = temp;

            var tempAnsw = answers[j];
            answers[j] = answers[i];
            answers[i] = tempAnsw;
        }

        private void EliminateRow(int fromInd, int elimInd)
        {
            double coef = equations[elimInd][fromInd] / equations[fromInd][fromInd];
            for (int j = 0; j < size; ++j)
                equations[elimInd][j] -= equations[fromInd][j] * coef;
            answers[elimInd] -= answers[fromInd] * coef;
        }
    }
}
