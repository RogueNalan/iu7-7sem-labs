﻿namespace lab3
{
    partial class lab3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnSetStates = new System.Windows.Forms.Button();
            this.dgvStates = new System.Windows.Forms.DataGridView();
            this.nudStates = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.dgvResults = new System.Windows.Forms.DataGridView();
            this.btnRandom = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResults)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Количество состояний:";
            // 
            // btnSetStates
            // 
            this.btnSetStates.Location = new System.Drawing.Point(262, 5);
            this.btnSetStates.Name = "btnSetStates";
            this.btnSetStates.Size = new System.Drawing.Size(75, 23);
            this.btnSetStates.TabIndex = 2;
            this.btnSetStates.Text = "Задать";
            this.btnSetStates.UseVisualStyleBackColor = true;
            this.btnSetStates.Click += new System.EventHandler(this.btnSetStates_Click);
            // 
            // dgvStates
            // 
            this.dgvStates.AllowUserToAddRows = false;
            this.dgvStates.AllowUserToDeleteRows = false;
            this.dgvStates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStates.Location = new System.Drawing.Point(8, 43);
            this.dgvStates.Name = "dgvStates";
            this.dgvStates.RowHeadersWidth = 60;
            this.dgvStates.Size = new System.Drawing.Size(520, 150);
            this.dgvStates.TabIndex = 3;
            // 
            // nudStates
            // 
            this.nudStates.Location = new System.Drawing.Point(136, 8);
            this.nudStates.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudStates.Name = "nudStates";
            this.nudStates.Size = new System.Drawing.Size(120, 20);
            this.nudStates.TabIndex = 4;
            this.nudStates.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 230);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Результаты:";
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(187, 199);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(210, 23);
            this.btnCalculate.TabIndex = 6;
            this.btnCalculate.Text = "Рассчитать финальные вероятности";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // dgvResults
            // 
            this.dgvResults.AllowUserToAddRows = false;
            this.dgvResults.AllowUserToDeleteRows = false;
            this.dgvResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResults.Location = new System.Drawing.Point(16, 246);
            this.dgvResults.Name = "dgvResults";
            this.dgvResults.ReadOnly = true;
            this.dgvResults.Size = new System.Drawing.Size(512, 74);
            this.dgvResults.TabIndex = 7;
            // 
            // btnRandom
            // 
            this.btnRandom.Location = new System.Drawing.Point(343, 5);
            this.btnRandom.Name = "btnRandom";
            this.btnRandom.Size = new System.Drawing.Size(185, 23);
            this.btnRandom.TabIndex = 8;
            this.btnRandom.Text = "Заполнить случаными числами";
            this.btnRandom.UseVisualStyleBackColor = true;
            this.btnRandom.Click += new System.EventHandler(this.btnRandom_Click);
            // 
            // lab3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 332);
            this.Controls.Add(this.btnRandom);
            this.Controls.Add(this.dgvResults);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nudStates);
            this.Controls.Add(this.dgvStates);
            this.Controls.Add(this.btnSetStates);
            this.Controls.Add(this.label1);
            this.Name = "lab3";
            this.Text = "lab3";
            this.Load += new System.EventHandler(this.lab3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResults)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSetStates;
        private System.Windows.Forms.DataGridView dgvStates;
        private System.Windows.Forms.NumericUpDown nudStates;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.DataGridView dgvResults;
        private System.Windows.Forms.Button btnRandom;
    }
}

